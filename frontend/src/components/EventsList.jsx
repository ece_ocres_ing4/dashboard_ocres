import { useState, useEffect } from "react";
import FullCalendar, { formatDate } from "@fullcalendar/react";
import {
  Box,
  List,
  ListItem,
  IconButton,
  ListItemText,
  Typography,
  useTheme,
} from "@mui/material";
import { tokens } from "../theme";
import DeleteIcon from '@mui/icons-material/Delete';

const EventsList = (props) => {
  const theme = useTheme();
  console.log("props", props);
  const colors = tokens(theme.palette.mode);
  const [mockdeleteEvent, setmockdeleteEvent] = useState([]);

  const deleteEvent = async (eventId) => {
    console.log("id", eventId)
    await fetch(`http://localhost:3000/api/delete/${eventId}`, {
      method: 'DELETE'
    })
      .then((response) => response.json())
      .then((eventId) => console.log("deleted", eventId))
      .catch((error) => console.error(error));
      eventId.remove();
  }

  return (
    <Box
      flex="1 1 20%"
      backgroundColor={colors.primary[400]}
      p="15px"
      borderRadius="4px"
    >
      <Typography variant="h5" padding-bottom="30px">Events</Typography>
      <List>
        {props.mockdataEvents.map((event) => (
          <ListItem
            key={event.id}
            sx={{
              backgroundColor: colors.greenAccent[600],
              margin: "10px 0",
              borderRadius: "2px",
              ":hover": {
                backgroundColor: colors.redAccent[600],
              },
            }}
          >
            <ListItemText
              primary={event.title}
              secondary={
                <Typography>
                  {formatDate(event.start, {
                    year: "numeric",
                    month: "short",
                    day: "numeric",
                  })}
                  - id:
                  {event.id}

                </Typography>
              }
            />
            <IconButton
              sx={{
                ":hover": {
                  color: colors.redAccent[400],
                },
              }}
              aria-label="delete"
              onClick={() => deleteEvent(event.id)}
            >
              <DeleteIcon />
            </IconButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );
};

export default EventsList;