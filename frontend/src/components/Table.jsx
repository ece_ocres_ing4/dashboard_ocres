import { useState, useEffect } from "react";
import { Box, useTheme } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import Header from "./Header";
import { tokens } from "../theme";


function Table(props){

    const theme = useTheme();
    const colors = tokens(theme.palette.mode);  
    const token_API = 'a11fabc023494051ad27977b783b55cc';
    const [mockData, setmockData] = useState([]);
    const columns = [
      {
        field: "name",
        headerName: "Name",
        flex: 1,
        cellClassName: "name-column--cell",
      },
      {
        field: "team",
        headerName: "Team",
        headerAlign: "left",
        align: "left",
        flex: 1,
      },
      {
        field: "nationality",
        headerName: "Nationality",
        flex: 1,
        headerAlign: "left",
        align: "left",
      },
      {
        field: "goals",
        headerName: "Goals",
        type: "number",
        flex: 0.25,
        headerAlign: "left",
        align: "left",
      },
    ];

    useEffect(() => {
        //Runs only on the first render
        async function fetchData() {
          const requestOptions = {
            headers: { 'X-Auth-Token': token_API },
        };
        var tmp = [];
        await fetch('https://api.football-data.org/v2/competitions/'+ props.selectedValue +'/scorers', requestOptions)
            .then(response => response.json()).then(response => {
              response.scorers.map((football_scorer, index) => {
                tmp = [...tmp, {
                  id: index+1,
                  name: football_scorer.player.name,
                  //ajouter l'ensemble des données via le chemin en console
                  team: football_scorer.team.name,
                  nationality: football_scorer.player.nationality,
                  goals: football_scorer.numberOfGoals,
                }];
              }
              );
            })
            setmockData(tmp);
        }
        fetchData();
      }, [props]);

    return(
        <Box m="10px">
      <Header
        subtitle="Top scorers"
      />
      <Box
        m="10px 0 0 0"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
            width: "30vw",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300],
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.blueAccent[700],
            borderBottom: "none",
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiCheckbox-root": {
            color: `${colors.greenAccent[200]} !important`,
          },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
            color: `${colors.grey[100]} !important`,
          },
        }}
      >
        <DataGrid
          rows={mockData}
          columns={columns}
          components={{ Toolbar: GridToolbar }}
        />
      </Box>
    </Box>
    )
}

export default Table;