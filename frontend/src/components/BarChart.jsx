import { useState, useEffect } from "react";
import { ResponsiveBar } from "@nivo/bar";
import { tokens } from "../theme";
import { Box, useTheme } from "@mui/material";


const BarChart = (props) => {
  const theme = useTheme();
  console.log("props",)
  const colors = tokens(theme.palette.mode);
  const token_API = 'a11fabc023494051ad27977b783b55cc';
  const [mockDataScorers, setmockDataScorers] = useState([]);

  useEffect(() => {
      //Runs only on the first render
      async function fetchDataScorers() {
        const requestOptions = {
          headers: { 'X-Auth-Token': token_API },
      };
      var tmp = [];
      await fetch('https://api.football-data.org/v2/competitions/'+ props.selectedValue +'/scorers', requestOptions)
          .then(response => response.json())
          .then(response => {
            response.scorers.map(
              (football_scorers) => {
              tmp = [...tmp, {
                name: football_scorers.player.name,
                goals: football_scorers.numberOfGoals,
                goalsColor: "hsl(110, 70%, 50%)",
              }];
            }
            );
          })
          setmockDataScorers(tmp);
        }
      fetchDataScorers();
    }, [props]);

  
  return (
    <ResponsiveBar
      data={mockDataScorers}
      theme={{
        // added
        axis: {
          domain: {
            line: {
              stroke: colors.grey[100],
            },
          },
          legend: {
            text: {
              fill: colors.grey[100],
            },
          },
          ticks: {
            line: {
              stroke: colors.grey[100],
              strokeWidth: 1,
            },
            text: {
              fill: colors.grey[100],
            },
          },
        },
        legends: {
          text: {
            fill: colors.grey[100],
          },
        },
      }}
      keys={["goals"]}
      indexBy="name"
      layout= 'horizontal'
      margin={{ top: 25, right: 130, bottom: 50, left: 150 }}
      padding={0.25}
      valueScale={{ type: "linear" }}
      indexScale={{ type: "band", round: true }}
      colors={{ scheme: "nivo" }}
      defs={[
        {
          id: "dots",
          type: "patternDots",
          background: "inherit",
          color: "#38bcb2",
          size: 4,
          padding: 1,
          stagger: true,
        },
        {
          id: "lines",
          type: "patternLines",
          background: "inherit",
          color: "#eed312",
          rotation: -45,
          lineWidth: 6,
          spacing: 10,
        },
      ]}
      borderColor={{
        from: "color",
        modifiers: [["darker", "1.6"]],
      }}
      axisTop={null}
      axisRight={null}
      axisBottom={{
        tickSize: 5,
        tickPadding: 1,
        tickRotation: 0,
        legend: props.isDashboard ? undefined : "goals", // changed
        legendPosition: "middle",
        legendOffset: 32,
      }}
      axisLeft={{
        tickSize: 1,
        tickPadding: 5,
        tickRotation: 0,
        legend: props.isDashboard ? undefined : "name", // changed
        legendPosition: "middle",
        legendOffset: -125,
      }}
      enableLabel={false}
      labelSkipWidth={12}
      labelSkipHeight={12}
      labelTextColor={{
        from: "color",
        modifiers: [["darker", 1.6]],
      }}
      legends={[
        {
          dataFrom: "keys",
          anchor: "bottom-right",
          direction: "column",
          justify: false,
          translateX: 120,
          translateY: 0,
          itemsSpacing: 2,
          itemWidth: 100,
          itemHeight: 20,
          itemDirection: "left-to-right",
          itemOpacity: 0.85,
          symbolSize: 20,
          effects: [
            {
              on: "hover",
              style: {
                itemOpacity: 1,
              },
            },
          ],
        },
      ]}
      role="application"
      barAriaLabel={function (e) {
        return e.id + ": " + e.formattedValue + " in country: " + e.indexValue;
      }}
    />
  );
};

export default BarChart;
