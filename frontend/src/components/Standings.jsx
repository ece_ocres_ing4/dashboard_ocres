import { useState, useEffect } from "react";
import { Box, useTheme } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import Header from "./Header";
import { tokens } from "../theme";


function Standings(props){

    const theme = useTheme();
    const colors = tokens(theme.palette.mode);  
    const token_API = 'a11fabc023494051ad27977b783b55cc';
    const [mockDataStandings, setmockDataStandings] = useState([]);
    const columns = [
      {
        field: "position",
        headerName: "Position",
        headerAlign: "left",
        align: "left",
        type: "number",
        flex: 0.25,
      },
      {
        field: "badge",
        headerName: "Badge",
        headerAlign: "left",
        align: "left",
        renderCell: (params) => <img src={params.value} style={{width:'30px'}}/>, // renderCell will render the component
        flex: 0.5,
      },
      {
        field: "name",
        headerName: "Name",
        flex: 1,
        cellClassName: "name-column--cell",
      },
      {
        field: "played",
        headerName: "Played",
        headerAlign: "left",
        align: "left",
        type: "number",
        flex: 0.25,
      },
      {
        field: "points",
        headerName: "Points",
        headerAlign: "left",
        align: "left",
        type: "number",
        flex: 0.25,
      },
    ];

    useEffect(() => {
        //Runs only on the first render
        async function fetchDataStandings() {
          const requestOptions = {
            headers: { 'X-Auth-Token': token_API },
        };
        var tmp = [];
        await fetch('https://api.football-data.org/v2/competitions/'+ props.selectedValue +'/standings', requestOptions)
            .then(response => response.json())
            .then(response => {
              response.standings[0].table.map(
                (football_standings, index) => {
                // console.log(football_standings);
                tmp = [...tmp, {
                  id: index,
                  position: football_standings.position,
                  badge: football_standings.team.crestUrl,
                  name: football_standings.team.name,
                  played: football_standings.playedGames,
                  points: football_standings.points,
                }];
              }
              );
            })
            setmockDataStandings(tmp);
        }
        fetchDataStandings();
      }, [props]);

    return(
    <Box m="10px">
      <Header
        subtitle="Standing"
      />
      <Box
        m="10px 0 0 0"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
            width: "40vw",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300],
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.blueAccent[700],
            borderBottom: "none",
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiCheckbox-root": {
            color: `${colors.greenAccent[200]} !important`,
          },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
            color: `${colors.grey[100]} !important`,
          },
        }}
      >
        <DataGrid
          rows={mockDataStandings}
          columns={columns}
          components={{ Toolbar: GridToolbar }}
        />
      </Box>
    </Box>
    )
}

export default Standings;