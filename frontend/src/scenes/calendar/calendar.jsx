import { useState, useEffect } from "react";
import FullCalendar, { formatDate } from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import listPlugin from "@fullcalendar/list";
import {
  Box,
  Typography,
  useTheme,
} from "@mui/material";
import Header from "../../components/Header";
import { tokens } from "../../theme";
import EventsList from "../../components/EventsList";


const Calendar = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const [mockDataEvents, setmockDataEvents] = useState([]);

  const generateId = () => {
    return Math.random().toString(16).substr(2, 4);
  };

  const handleDateClick = async (selected) => {
    const title = prompt("Please enter a title for your event");
    const calendarApi = selected.view.calendar;
    calendarApi.unselect();

    if (title) {
      const identifiant = generateId();
      const data = {
        id: identifiant,
        title,
        start: selected.startStr,
        end: selected.endStr,
        date: selected.allDay,
      };
      console.log("oyi1", data);
      console.log("oyi2", selected);
      await fetch('http://localhost:3000/api/post', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: data.id,
          title: data.title,
          start: data.start,
          end: data.end,
          date: data.date
        })
      })
        .then((response) => response.json())
        .then((data) => {
          let tmp = mockDataEvents;
          tmp.push(data);
          setmockDataEvents(tmp);
        })
        .catch((error) => console.error(error));
    };
  }

  useEffect(() => {
    async function fetchDataEvents() {
      var tmp = [];
      await fetch('http://localhost:3000/api/getAll')
        .then(response => response.json())
        .then(response => {
          response.map(
            (event) => {
              tmp = [...tmp, {
                id: event._id,
                title: event.title,
                start: event.start,
                end: event.end,
                date: event.date,
              }];
            }
          );
        })
      setmockDataEvents(tmp);
    }
    fetchDataEvents();
  }, [mockDataEvents]);

  return (
    <Box m="20px" position="relative" left="21vw" width="75vw">
      <Header title="Calendar" subtitle="Full Calendar Interactive Page" />
      <Box display="flex" justifyContent="space-between">
        {/* CALENDAR SIDEBAR */}
        <Box
          flex="1 1 40%"
          backgroundColor={colors.primary[400]}
          p="15px"
          borderRadius="4px"
          overflowY= "auto"
        >
          <EventsList mockdataEvents={mockDataEvents} />
        </Box>

        {/* CALENDAR */}
        <Box flex="1 1 100%" ml="15px">
          <FullCalendar
            height="75vh"
            plugins={[
              dayGridPlugin,
              timeGridPlugin,
              interactionPlugin,
              listPlugin,
            ]}
            headerToolbar={{
              left: "prev,next today",
              center: "title",
              right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth",
            }}
            initialView="dayGridMonth"
            editable={true}
            selectable={true}
            selectMirror={true}
            dayMaxEvents={true}
            select={handleDateClick}
            // eventsSet={(events) => setmockDataEvents(events)}
            initialEvents={mockDataEvents}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default Calendar;
