import { Box, Button, Select, InputLabel, MenuItem, FormControl, Typography, useTheme } from "@mui/material";
import { tokens } from "../../theme";
import { useState, useEffect } from "react";
import Header from "../../components/Header";
import BarChart from "../../components/BarChart";
import Table from "../../components/Table";
import Standings from "../../components/Standings";
import EventsList from "../../components/EventsList";

const Dashboard = () => {

  const [selectedValue, setSelectedValue] = useState('PL');
  const [mockDataEvents, setmockDataEvents] = useState([]);

  function handleChange(event) {
    console.log(event.target.value); 
    console.log(typeof event.target.value); 
    setSelectedValue(event.target.value);
    console.log('https://api.football-data.org/v2/competitions/'+ event.target.value + '/scorers'); // log the changing value
  }

  const theme = useTheme();
  const colors = tokens(theme.palette.mode);  

  useEffect(() => {
    async function fetchDataEvents() {
      var tmp = [];
      await fetch('http://localhost:3000/api/getAll')
        .then(response => response.json())
        .then(response => {
          response.map(
            (event) => {
              tmp = [...tmp, {
                id: event._id,
                title: event.title,
                start: event.start,
                end: event.end,
                date: event.date,
              }];
            }
          );
        })
      setmockDataEvents(tmp);
    }
    fetchDataEvents();
  }, [mockDataEvents]);

  return (
    <Box m="20px" position="relative" left="21vw">
      {/* HEADER */}
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Header title="DASHBOARD" />
      </Box>
       {/* Buttons Select */}
      <Box sx={{ minWidth: 250 }}>
      <FormControl>
        <InputLabel id="demo-simple-select-label">League</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={selectedValue}
          displayEmpty
          onChange={handleChange}
          label="Championnat"
        >
          <MenuItem value={'PL'}>Premier League</MenuItem>
          <MenuItem value={'SA'}>Serie A</MenuItem>
          <MenuItem value={'PD'}>Primera Division</MenuItem>
          <MenuItem value={'BL1'}>Bundesliga</MenuItem>
          <MenuItem value={'FL1'}>Ligue 1</MenuItem>
        </Select>
      </FormControl>
    </Box>

    {/* ROW 0 */}
    <Box
    display="flex" alignItems="center"
    >
      <Table selectedValue={selectedValue}/>
      <Standings selectedValue={selectedValue}/>
    </Box>

    {/* GRID & CHARTS */}
    <Box
      display="grid"
      gridTemplateColumns="repeat(12, 1fr)"
      gridAutoRows="140px"
      gap="20px"
      paddingBottom="25px"
      paddingTop="15px"
    >

      {/* ROW 3 */}
      <Box
        gridColumn="span 5"
        gridRow="span 2"
        backgroundColor={colors.primary[400]}
      >
        <Typography
          variant="h5"
          fontWeight="600"
          sx={{ padding: "30px 30px 0 30px" }}
        >
          Top scorers
        </Typography>
        <Box height="35vh" width="42vw" mt="-20px" overflowY= "auto">
          <BarChart isDashboard={false} selectedValue={selectedValue}/>
        </Box>
      </Box>

      <Box
        gridColumn="span 4"
        gridRow="span 2"
        backgroundColor={colors.primary[400]}
        sx={{ padding: "30px 30px 0px 30px",
              overflowY: "auto" }}
      >
          <EventsList mockdataEvents={mockDataEvents}/>
      </Box>
    </Box>
  </Box>
  );
};

export default Dashboard;
