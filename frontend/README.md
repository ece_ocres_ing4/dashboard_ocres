React Dashboard Project - OCRES 8

Football Application
View Top5 Biggest League ranking, top scorers and book an event into your calendar to never miss a game !

Thomas LOPEZ - Slim GAESSLER

Installation and Setup Instructions

We used - React, Express, Node, Mongo DB.

Example:
Clone down this repository. You will need node and npm installed globally on your machine.

Installation Frontend :

npm install
(npm i)

To Run Test Suite:

npm test

To Start Frontend :

npm start

To Visit App Frontend:

localhost:5000


Installation Backend :

npm install
(npm i)

To Start Backend :

node App.js

To Visit App Backend:

localhost:3000
