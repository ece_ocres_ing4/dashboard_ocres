const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema({
    id: {
        require: true,
        type: String
    },
    title: {
        required: true,
        type: String
    },
    start: {
        required: true,
        type: Date
    },
    end: {
        required: false,
        type: Date
    },
    date: {
        required: true,
        type: Boolean
    }
})

module.exports = mongoose.model('Data', dataSchema)