require('dotenv').config();

const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const mongoString = process.env.DATABASE_URL

const app = express();
const port = 3000;
app.use(cors());

mongoose.connect(mongoString);
const database = mongoose.connection;

database.on('error', (error) => {
    console.log(error)
})

database.once('connected', () => {
    console.log('Database Connected');
})

app.use(express.json());

const routes = require('./routes/routes');
app.use('/api', routes)

app.get('/', (req, res) => {
  res.send('The backend is working !')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
